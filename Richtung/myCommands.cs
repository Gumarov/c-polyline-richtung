﻿using System;
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
using System.Collections.Generic;
using Autodesk.AutoCAD.Colors;

namespace Richtung
{
    public class MyCommands
    {
        static Document doc = Application.DocumentManager.MdiActiveDocument;
        Editor ed = doc.Editor;
        Database db = doc.Database;
        int Pfeilgrosse = 5;

        [CommandMethod("Richtung")]
        public void MyCommand()
        {

            PromptEntityOptions peo = new PromptEntityOptions("\nEine linie Wählen:");
            peo.SetRejectMessage("\nnur Linien!");
            peo.AddAllowedClass(typeof(Polyline), false);
            //peo.AddAllowedClass(typeof(Polyline2d), false);
            peo.AddAllowedClass(typeof(Polyline3d), false);
            //peo.AddAllowedClass(typeof(Line), false);

            PromptEntityResult per = ed.GetEntity(peo);
            if (per.Status != PromptStatus.OK)
                return;
            ObjectId myId;
            Transaction tr = doc.TransactionManager.StartTransaction();
            using (tr)
            {
                List<Point2d> Punkte2d = new List<Point2d>();
                DBObject myLine = tr.GetObject(per.ObjectId, OpenMode.ForRead) as DBObject;
                if (myLine.GetRXClass().Name == "AcDbPolyline")
                {
                    Polyline myPolyline = myLine as Polyline;
                    //for (int i = 0; i < myPolyline.NumberOfVertices; i++)
                    //{
                    //    myPolyline.GetPointAtDist(10);
                    //    Punkte2d.Add(myPolyline.GetPoint2dAt(i));
                    //}
                    for (int i = 0; i <= myPolyline.Length; i += Pfeilgrosse)
                    {
                        Point3d po3d = myPolyline.GetPointAtDist(i);
                        Point2d point = new Point2d(po3d.X, po3d.Y);
                        Punkte2d.Add(point);
                    }
                }
                if (myLine.GetRXClass().Name == "AcDb3dPolyline" )
                {
                    Polyline3d myPolyline = myLine as Polyline3d;
                    for(int i = 0; i <= myPolyline.Length; i += Pfeilgrosse)
                    {
                        Point3d po3d = myPolyline.GetPointAtDist(i);
                        Point2d point = new Point2d(po3d.X, po3d.Y);
                        Punkte2d.Add(point);
                    }
                }


                BlockTable bt = tr.GetObject(db.BlockTableId, OpenMode.ForRead) as BlockTable;
                BlockTableRecord btr = tr.GetObject(bt[BlockTableRecord.ModelSpace], OpenMode.ForWrite) as BlockTableRecord;
                using (Polyline newPolyline = new Polyline())
                {
                    for (int i=0; i < Punkte2d.Count; i++)
                    {
                        newPolyline.AddVertexAt(i, Punkte2d[i], 0, Pfeilgrosse, 0);
                    }
                    newPolyline.Color = Color.FromColorIndex(ColorMethod.ByAci, 1);
                    btr.AppendEntity(newPolyline);
                    tr.AddNewlyCreatedDBObject(newPolyline, true);
                    myId = newPolyline.ObjectId;
                }
                    tr.Commit();
            }
            PromptResult pr = ed.GetString("\nGut!");
            erase(myId);

        }
        public void erase(ObjectId myId)
        {
            Transaction tr = db.TransactionManager.StartTransaction();
            using (tr)
            {
                var Line = tr.GetObject(myId, OpenMode.ForWrite);
                Line.Erase();
                tr.Commit();
            }
        }
    }
}


